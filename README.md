# Data Engineer - Challenge
This is the Basic Challenge for Data Engineers.

Lider.cl wants to create a new shiny section for videogames, to bring custom videogame information to our clients, our Analytics team needs a new report each day with several videogames information. This information will be used to create a lot of ML models & Data Science to give our customers the best experience and make the best decision of which videogame buy.

For this Challenge, we want you to do a Job who give us the Data for the Analytics team, but with a few concerns:
- The Job must be an ETL code in Java or Scala or Python.
- We need the Data Model for the problem.
- And we want a Deployment for this code.

## ETL Job
The job must receive the datasets & brings a few things:
- The top 10 best games for each console/company.
- The worst 10 games for each console/company.
- The top 10 best games for all consoles.
- The worst 10 games for all consoles.
The data is in the folder data/ in the root. The report can be exposed in any way you want, but remember this is an ETL Job.

## Data Model
The Data Model must be in 3NF. 
Save the model in the DataModel folder in both formats (data model format & JPG/PNG).

- Please, see the file 'model.sql' in 'DataModel' folder.

# Development and Deployment

Python, Jupyter Notebook, GCP, Docker and Flask were used to build the ETL and deployment. There were several ways to create the ETL, but I chose the fastest to implement. The ETL could have been created in Google Cloud Function with Scheduler, but it could not have been displayed through the repository. Another option is to create the ETL through Cloud Composer with Airflow, but it is somewhat complicated for the type of problem to be solved.

In the first instance the data model was created through GCP. Bigquery was used to create the schema, one VM instance was created on Compute Engine to store the files, and Docker was used to contain the code. 

IMPORTANT: All the steps to follow are under the GCP context. The idea is to use the same environment to avoid communication problems between machines.

To create the model in 3FN, Dbeaver with Postgres was used. It should be remembered that Bigquery does not allow you to create models with primary or foreign keys.

## Steps:
- Create VM instance on Compute Engine. This will be use to create the code and deploy the app.
- Install Jupyter Notebook environment through Anaconda in the VM instance.
- Configurate Google SDK to access through SSH to Jupyter Notebook.
- Install Docker.
- Create model in BigQuery or Postgres. Please, see the file 'model.sql' in 'DataModel' folder.
- Create ETL on Jupyter Notebook. If you have troubles with GCP credential, please configurate it in the ipynb file. You probably need a GCP service account file.
- Create extractor to query the loaded information on BigQuery.
- Create a Flask server to read both files, ETL.py and get_data_to_answer.py. These files were created from the Jupyter Notebooks used. The first file is in charge of reading, extracting, manipulating and loading the information from the CSV files to BigQuery.
The second file is in charge of consuming this information and generating the answers to the questions about Video Games. 
- Create a Dockerfile to deploy the Flask app and put in the root folder.
- Build the Docker container:

```
docker build -t etl-process .
```
- Run the Docker container to expose the app:

```
docker run -d -p 5000:5000 container-id
```
It's important to know that container-id is the id of the recently created Docker container. I use Kitematic Tool to see the differents running container.
The application should be running at the address [http://0.0.0.0:5000/](http://0.0.0.0:5000/) through the Flask server.

Once the container is running, please enter the specified URL in web browser. The results will appear in the shell. I left some screenshots in 'Screenshot Answers' folder.

To calculate the best and worst game, the variable "metascore" was used. 
This decision was made based on the link: [Metacritic - Metascores](https://www.metacritic.com/about-metascores)


## Datasets
We use the data from TopGames provided by Metascore.

* [Kaggle: Metacritic reviewed games since 2000](https://www.kaggle.com/destring/metacritic-reviewed-games-since-2000)




