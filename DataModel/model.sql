------------------------------------------------------------------------------------------------------
# PostgreSQL
------------------------------------------------------------------------------------------------------

## Se creó en Postgres un modelo de datos que corresponde a la forma 3FN. 
## Para crear el esquema, es necesario correr todas estas consultas en un cliente de base de datos.

CREATE TABLE walmart_challenge.company (
	id INT NOT NULL AUTO_INCREMENT,
	company_name varchar(50) NOT NULL,
	CONSTRAINT company_PK PRIMARY KEY (id)
);

CREATE TABLE walmart_challenge.consoles (
	id INT NOT NULL AUTO_INCREMENT,
	id_company INT NOT NULL,
	console_name VARCHAR(50) NULL,
	CONSTRAINT consoles_PK PRIMARY KEY (id),
	CONSTRAINT consoles_FK FOREIGN KEY (id_company) REFERENCES walmart_challenge.company(id) ON DELETE CASCADE
);

CREATE TABLE walmart_challenge.game (
	id INT NOT NULL AUTO_INCREMENT,
	id_console INT NOT NULL,
	game_name VARCHAR(100) NULL,
	CONSTRAINT game_PK PRIMARY KEY (id),
	CONSTRAINT game_FK FOREIGN KEY (id_console) REFERENCES walmart_challenge.consoles(id) ON DELETE CASCADE
);

CREATE TABLE walmart_challenge.result (
	id INT NOT NULL AUTO_INCREMENT,
	metascore INT NULL,
	userscore FLOAT NULL,
	id_game INT NOT NULL,
	id_console INT NOT NULL,
	`date` TIMESTAMP NULL,
	CONSTRAINT result_PK PRIMARY KEY (id,id_game,id_console),
	CONSTRAINT result_FK FOREIGN KEY (id_console) REFERENCES walmart_challenge.consoles(id),
	CONSTRAINT result_FK_1 FOREIGN KEY (id_game) REFERENCES walmart_challenge.game(id)
);

ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_0900_ai_ci;

------------------------------------------------------------------------------------------------------
# BigQuery
------------------------------------------------------------------------------------------------------

## Como BigQuery es una arquitectura datalake, no es necesario construir las tablas con su PK y FK. Sólo se debe entender el modelo a crear.
## Este código fue usado en el Pipeline para crer el esquema antes de trabajar los datos.

CREATE OR REPLACE TABLE `proyecto_gcp.dataset.company` (
	id INT64 NOT NULL,
	company_name STRING NOT NULL
);

CREATE OR REPLACE TABLE `proyecto_gcp.dataset.console` (
	id INT64 NOT NULL,
	console_name STRING NOT NULL
);

CREATE OR REPLACE TABLE `proyecto_gcp.dataset.game` (
	id INT64 NOT NULL,
	id_console INT64 NOT NULL,
	game_name STRING,
);

CREATE OR REPLACE TABLE `proyecto_gcp.dataset.result` (
	id INT64 NOT NULL,
	metascore INT64,
	userscore FLOAT64,
	id_game INT64 NOT NULL,
	id_console INT64 NOT NULL,
	`date` TIMESTAMP
);
