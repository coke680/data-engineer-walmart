FROM ubuntu:18.04

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev

WORKDIR /project

COPY Deployment/ .

COPY data/ .

RUN ls /project 

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD [ "python", "index.py" ]