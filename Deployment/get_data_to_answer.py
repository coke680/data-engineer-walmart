#!/usr/bin/env python
# coding: utf-8

# ### Respuestas - Walmart
# - Este código genera los resultados a las preguntas:
#     - The top 10 best games for each console/company.
#     - The worst 10 games for each console/company.
#     - The top 10 best games for all consoles.
#     - The worst 10 games for all consoles.

# In[1]:


# importar librerías
import pandas as pd
import numpy as np

from google.cloud import bigquery
from datetime import datetime


# ### Configurar GCP

# In[2]:


# si el entorno de GCP es el mismo en donde se ejecuta el código, sólo basta con dejar lo siguiente.
client = bigquery.Client()

# si no, configurar lo de abajo

## Acá debería estar la credencial a utilizar, generalmente una cuenta de servicio en GCP
#key_path = "cuenta-servicio.json"

#credentials = service_account.Credentials.from_service_account_file(
#  key_path, scopes=["https://www.googleapis.com/auth/cloud-platform"],
#)

## Acá se inicializa el cliente con las credenciales
#client = bigquery.Client(credentials=credentials, project=credentials.project_id,) 


# In[3]:


#definimos las variables de entorno para GCP
PROJECT = 'GCP_project' 
DATASET = 'dataset'


# ### Consultar tablas en BigQuery.

# - Para calcular el mejor y peor juego, se usó la variable **"metascore"**. Esta decisión se tomó en base al link: https://www.metacritic.com/about-metascores 

# In[4]:


sql = """
    SELECT game_name,
       console_name,
       metascore,
    FROM (
      SELECT
          game_name,
          console_name,
          metascore,
          row_number() OVER (PARTITION BY console_name ORDER BY metascore DESC) as n
      FROM `{}.{}.result` a
      INNER JOIN `{}.{}.game` b
      ON a.id_game = b.id
      INNER JOIN `{}.{}.console` c
      ON a.id_console = c.id
      ORDER BY
          console_name
    )
    WHERE n <= 10
""".format(PROJECT, DATASET, PROJECT, DATASET, PROJECT, DATASET)

top_10_best_games_console_company = client.query(sql).to_dataframe()


# In[5]:


print(top_10_best_games_console_company.head(15))


# In[6]:


sql = """
    SELECT game_name,
       console_name,
       metascore,
    FROM (
      SELECT
          game_name,
          console_name,
          metascore,
          row_number() OVER (PARTITION BY console_name ORDER BY metascore ASC) as n
      FROM `{}.{}.result` a
      INNER JOIN `{}.{}.game` b
      ON a.id_game = b.id
      INNER JOIN `{}.{}.console` c
      ON a.id_console = c.id
      ORDER BY
          console_name
    )
    WHERE n <= 10
""".format(PROJECT, DATASET, PROJECT, DATASET, PROJECT, DATASET)

top_10_worst_games_console_company = client.query(sql).to_dataframe()


# In[7]:


print(top_10_worst_games_console_company.head(15))


# In[8]:


sql = """
   SELECT game_name, console_name, metascore 
   FROM `{}.{}.result` a
   INNER JOIN `{}.{}.game` b
   ON a.id_game = b.id
   INNER JOIN `{}.{}.console` c
   ON a.id_console = c.id
   ORDER BY metascore DESC
   LIMIT 10;
""".format(PROJECT, DATASET, PROJECT, DATASET, PROJECT, DATASET)

top_10_best_games = client.query(sql).to_dataframe()


# In[9]:


print(top_10_best_games)


# In[10]:


sql = """
    SELECT game_name, console_name, metascore 
    FROM `{}.{}.result` a
    INNER JOIN `{}.{}.game` b
    ON a.id_game = b.id
    INNER JOIN `{}.{}.console` c
    ON a.id_console = c.id
    ORDER BY metascore ASC
    LIMIT 10
""".format(PROJECT, DATASET, PROJECT, DATASET, PROJECT, DATASET)

top_10_worst_games = client.query(sql).to_dataframe()


# In[11]:


print(top_10_worst_games)

