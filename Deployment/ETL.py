#!/usr/bin/env python
# coding: utf-8

# ### ETL JOB - Walmart
# - Este código crea un esquema en BigQuery que luego se llena a partir de los archivos dados. Estos archivos son transformados, se crean las tablas separadas (de acuerdo a la 3FN) y se carga la información en el esquema creado en BigQuery. Para esto, ya tengo cargados los datos de mi cuenta de servicio de GCP.
# - Luego de cargar los datos en el esquema, se consultan las tablas, se hacemn los cruces necesarios y se generan los resultados a las preguntas:
#     - The top 10 best games for each console/company.
#     - The worst 10 games for each console/company.
#     - The top 10 best games for all consoles.
#     - The worst 10 games for all consoles.

# In[128]:


# importar librerías
import pandas as pd
import numpy as np

from google.cloud import bigquery
from datetime import datetime


# ### Configurar GCP

# In[129]:


# si el entorno de GCP es el mismo en donde se ejecuta el código, sólo basta con dejar lo siguiente.
client = bigquery.Client()

# si no, configurar lo de abajo

## Acá debería estar la credencial a utilizar, generalmente una cuenta de servicio en GCP
#key_path = "cuenta-servicio.json"

#credentials = service_account.Credentials.from_service_account_file(
#  key_path, scopes=["https://www.googleapis.com/auth/cloud-platform"],
#)

## Acá se inicializa el cliente con las credenciales
#client = bigquery.Client(credentials=credentials, project=credentials.project_id,) 


# In[130]:


#definimos las variables de entorno para GCP
PROJECT = 'GCP_project' 
DATASET = 'dataset'


# ### Se definen funciones para trabajar los datos

# In[131]:


# función para crear un esquema en BigQuery, de acuerdo a lo trabajado en 3FN
def build_schema():
    sql = """
        CREATE OR REPLACE TABLE `{}.{}.company` (
            id INT64 NOT NULL,
            company_name STRING NOT NULL
        );

        CREATE OR REPLACE TABLE `{}.{}.console` (
            id INT64 NOT NULL,
            console_name STRING NOT NULL
        );

        CREATE OR REPLACE TABLE `{}.{}.game` (
            id INT64 NOT NULL,
            id_console INT64 NOT NULL,
            game_name STRING,
        );

        CREATE OR REPLACE TABLE `{}.{}.result` (
            id INT64 NOT NULL,
            metascore INT64,
            userscore FLOAT64,
            id_game INT64 NOT NULL,
            id_console INT64 NOT NULL,
            `date` TIMESTAMP
        );
    """.format(PROJECT, DATASET, PROJECT, DATASET, PROJECT, DATASET, PROJECT, DATASET)
    query_job = client.query(sql)  # Llamada a la API.
    query_job.result()  # Ejecutar job.
    print("\nProceso terminado con éxito. Esquema creado en BigQuery.")
    
    
# funciones para transformar el dato
def transform_data_consoles(consoles):
    # cambiar formato consola
    consoles['console'] = consoles['console'].str.strip()
    # cambiar formato company name
    consoles['company'] = consoles['company'].str.strip()    
    return consoles

def transform_data_result(result):
    # cambiar formato de fecha a datetime
    result['date'] = pd.to_datetime(result['date'])
    # cambiar formato consola
    result['console'] = result['console'].str.strip()
    # cambiar formato game name
    result['name'] = result['name'].str.strip()
    # replace de los nombres que no correspondan. Ponemos 0 a los userscore = tbd
    # también se podría reemplazar con el promedio general o el promedio por consola.
    # por último, se convierte a float.
    result['userscore'] = result['userscore'].str.strip().replace('tbd','0').astype(float)    
    return result


# ### Se lee el archivo 'consoles.csv'

# In[132]:


consoles = pd.read_csv('consoles.csv', sep=",")
print(consoles.head())


# In[133]:


# tamaño del dataframe
print(consoles.shape)


# In[134]:


# se consultan los únicos por consola, para ver duplicados o errores en los nombres
print(consoles.console.unique())


# In[135]:


# se consultan los únicos por company, para ver duplicados o errores en los nombres
print(consoles.company.unique())


# In[136]:


# se aplica la función que transformará los datos
consoles = transform_data_consoles(consoles)


# In[137]:


# se observa el tipo de dato de cada columna del dataframe
print(consoles.dtypes)


# In[138]:


# se observan datos nulos
print(consoles.isnull().sum())


# ### Se lee el archivo 'result.csv'

# In[139]:


result = pd.read_csv('result.csv', sep=",")
print(result.sample(5))


# In[140]:


# se consultan los únicos por metascore, para ver duplicados o errores en los nombres
result.metascore.unique()


# In[141]:


# se consultan los únicos por metascore, para ver duplicados o errores en los nombres
print(result.userscore.unique())


# In[142]:


print(result.console.unique())


# In[143]:


# se observa si existen datos que correspondan con 'tbd'
result[result.name.str.contains('tbd')]


# In[144]:


# se aplica la función que transformará los datos
result = transform_data_result(result)


# In[145]:


# se observa el tipo de dato de cada columna del dataframe
print(result.dtypes)


# In[146]:


# se observan datos nulos
print(result.isnull().sum())


# In[147]:


# tamaño del dataframe
print(result.shape)


# ### Crear tablas 'company' y 'console' para guardar esquema final

# In[148]:


# se crea un dataframe a partir del original, se crea un sort y se resetea el index para guardar por orden alfabético
company = consoles[['company']].drop_duplicates().sort_values(by="company").reset_index()
console = consoles[['console']].drop_duplicates().sort_values(by="console").reset_index()
game = result[['name', 'console']].drop_duplicates().sort_values(by=["name","console"]).reset_index()

# se renombran las columnas
company.columns = ['id','company_name']
console.columns = ['id','console_name']
game.columns = ['id','game_name','console_name']

# nuevo id
company['id'] = company.index + 1
console['id'] = console.index + 1
game['id'] = game.index + 1


# In[149]:


print(game.head())


# In[150]:


# se crea la table 'game' final, añadiendo el id_console. De este modo se puede guardar en el esquema creado.
game_console = pd.merge(game, console, on="console_name", how="left")
game = game_console[['id_x','id_y','game_name']]
game.columns = ['id','id_console','game_name']


# In[151]:


print(game.head())


# ### Crear tabla 'result' a partir de las anteriores

# In[152]:


print(game_console.head())


# In[153]:


print(result.head())


# In[154]:


# se realiza un cruce con 2 llaves (juego, consola) para poder crear la tabla final de result.
result_game = pd.merge(result, game_console, left_on=["name","console"], right_on=["game_name","console_name"], how="left")


# In[155]:


# se revisa el resultado haciendo samples al dataframe
result_game.sample(5)
#print(result_game.head())


# In[156]:


# se crea el dataset final de result
result_final = result_game[['metascore', 'userscore','id_x','id_y','date']].reset_index()
# se renombran las columnas
result_final.columns = ['id','metascore', 'userscore','id_game','id_console','date']
# nuevo id para result
result_final['id'] = result_final.index + 1


# In[157]:


result_final.sample(5)


# In[158]:


# tamaño matriz del primer df 'result'
print(result.shape)


# In[159]:


# tamaño matriz del segundo df 'result'
print(result_final.shape)


# In[160]:


# se verifican los nulos
print(result_final.isnull().sum())


# - Si se comparan los dataframes originales con los nuevos, se ve que no se ha perdido ningún dato.

# ### Cargar datos en BigQuery

# In[161]:


# se aplica la función que creará el esquema
build_schema()


# In[162]:


table_id = PROJECT + '.' + DATASET + '.company'

job_config = bigquery.LoadJobConfig(schema=[

])

job = client.load_table_from_dataframe(
    company, table_id, job_config=job_config
)
# Wait for the load job to complete.
job.result()

destination_table = client.get_table(table_id)  # Make an API request.
print("Cargadas {} filas.".format(destination_table.num_rows))
print("\nProceso terminado con éxito. Tabla 'company' creada")


# In[163]:


table_id = PROJECT + '.' + DATASET + '.console'

job_config = bigquery.LoadJobConfig(schema=[

])

job = client.load_table_from_dataframe(
    console, table_id, job_config=job_config
)

# Wait for the load job to complete.
job.result()

destination_table = client.get_table(table_id)  # Make an API request.
print("Cargadas {} filas.".format(destination_table.num_rows))
print("\nProceso terminado con éxito. Tabla 'console' creada")


# In[164]:


table_id = PROJECT + '.' + DATASET + '.game'

job_config = bigquery.LoadJobConfig(schema=[

])

job = client.load_table_from_dataframe(
    game, table_id, job_config=job_config
)

# Wait for the load job to complete.
job.result()

destination_table = client.get_table(table_id)  # Make an API request.
print("Cargadas {} filas.".format(destination_table.num_rows))
print("\nProceso terminado con éxito. Tabla 'game' creada")


# In[165]:


table_id = PROJECT + '.' + DATASET + '.result'

job_config = bigquery.LoadJobConfig(schema=[

])

job = client.load_table_from_dataframe(
    result_final, table_id, job_config=job_config
)

# Wait for the load job to complete.
job.result()

destination_table = client.get_table(table_id)  # Make an API request.
print("Cargadas {} filas.".format(destination_table.num_rows))
print("\nProceso terminado con éxito. Tabla 'result' creada")


# In[ ]:




