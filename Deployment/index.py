from flask import Flask
app = Flask(__name__)

@app.route("/")
def main():

  etl_file = "ETL.py"
  exec(open(etl_file).read())

  query_file = "get_data_to_answer.py"
  exec(open(query_file).read())

  return 'Todo correcto!'

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')